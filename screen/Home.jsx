import { View, Text, StyleSheet } from "react-native";
import React from "react";
import { Avatar } from "react-native-elements";

export default function Home() {
  return (
    <View
      style={{
        flex: 1,
        backgroundColor: "gray",
      }}
    >
      <Text style={styles.titleScreen}>My Profile</Text>
      <View
        style={{
          flexDirection: "row",
        }}
      >
        <Avatar
          rounded
          source={require("../assets/Photos/fotoAghna.jpg")}
          size={150}
        />
        <View style={{flexWrap:'nowrap', flex:1}}>
          <Text
            style={{
              fontSize: 25,
              fontWeight: "bold",
            }}
          >
            Aghna Jaya Pranada
          </Text>
          <Text
            style={{
              fontSize: 20,
            }}
          >
            Im From Binus University Computer Science
          </Text>
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  titleScreen: {
    fontSize: 20,
    fontWeight: "600",
  },
});
