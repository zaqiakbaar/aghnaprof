import { NavigationContainer } from "@react-navigation/native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import Home from "./screen/Home";


const Stack=createNativeStackNavigator()

export default function AppStack(){
  return(
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen
        options={{
          headerShadowVisible:false,
        }}
        name='Home'
        component={Home}
        />
      </Stack.Navigator>
    </NavigationContainer>
  )
}